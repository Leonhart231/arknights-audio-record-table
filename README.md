# Arknights Audio Record Table
The goal of this project is to take Arknights' JSON file containing the operators' audio records and convert it into a CSV file that can be used by the Wiki editors. The Wiki requires filenames in the format `[operator name]-###.[filetype]`, where the number begins from 001 and the extension of mp3 should be used.

The game files are sourced from <https://github.com/Kengxxiao/ArknightsGameData>.
