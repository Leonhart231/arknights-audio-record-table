#!/usr/bin/env python3
# Copyright © 2023 Leonhart231
# SPDX-License-Identifier: GPL-3.0-or-later

# This script creates a CSV of all of the audio records from the global Arknights server.

import csv
import json
import requests

github_url = "https://github.com/Kengxxiao/ArknightsGameData/raw/master/en_US/gamedata/excel/"
character_file = "character_table.json"
audio_record_file = "charword_table.json"
output_file = "audio_records.csv"

def print_error(*args, **kwargs):
    print('Error:', *args, **kwargs)
    return

# This function downloads the needed game data files
def download_game_data():
    # Download the character data (needed to tie a character name to an ID)
    json_url = github_url + character_file
    json_req = requests.get(json_url, allow_redirects=True)
    open(character_file, 'wb').write(json_req.content)
    # Download the audio record data
    json_url = github_url + audio_record_file
    json_req = requests.get(json_url, allow_redirects=True)
    open(audio_record_file, 'wb').write(json_req.content)
    return

# This function forms the initial character dictionary
def form_character_dict():
    data_dict = []
    with open(character_file, "r") as f:
        data_json = json.load(f)
    for id in data_json:
        c = data_json[id]
        # Initialize an empty records list so it's present
        tmp = {"id": id, "name": c["name"], "records": []}
        data_dict.append(tmp)
    return data_dict

# This function populates the audio records in the character dictionary
def populate_character_dict(data_dict):
    with open(audio_record_file, "r") as f:
        data_json = json.load(f)
    data_json = data_json["charWords"]
    for key in data_json:
        record = data_json[key]
        id = record["charId"]
        record_id = record["charWordId"]
        title = record["voiceTitle"]
        text = record["voiceText"]
        asset = record["voiceAsset"]
        tmp = {"id": record_id, "title": title, "text": text, "asset": asset}
        entry = next((c for c in data_dict if c["id"] == id), None)
        if not entry:
            print_error("Character ID", id, "not found for voiceline", key)
        else:
            entry["records"].append(tmp)
    return

# This function does sanitization and processing of the character dictionary
def sanitize_character_dict(data_dict):
    # Remove characters with no audio records
    data_dict = [a for a in data_dict if len(a["records"]) != 0]
    # Shalem is duplicated for an unknown reason, this removes the invalid copy
    data_dict = [a for a in data_dict if a["id"] != "char_512_aprot"]
    # Check for duplicated characters
    for i in range(len(data_dict) - 1):
        for j in range(i + 1, len(data_dict)):
            if data_dict[i]["name"] == data_dict[j]["name"]:
                print_error("Ducplicate characters", data_dict[i]["name"])
    # Check for duplicated audio records
    for c in data_dict:
        for i in range(len(c["records"]) - 1):
            for j in range(i + 1, len(c["records"])):
                if c["records"][i]["id"] == c["records"][j]["id"]:
                    print_error("Duplicated audio record for", c["name"], ",", c["records"][i]["title"])
    return

# This function creates an Excel-compatible CSV file from the extracted data
def write_csv(data_dict):
    with open(output_file, "w", newline="") as f:
        writer = csv.writer(f, dialect="excel")
        for c in data_dict:
            record = 1
            for r in c["records"]:
                wiki_name = c["name"] + "-" + "{0:03}".format(record) + ".mp3"
                tmp = [c["name"], r["title"], r["text"], r["asset"], wiki_name]
                writer.writerow(tmp)
                record += 1
    return

download_game_data()
data_dict = form_character_dict()
populate_character_dict(data_dict)
sanitize_character_dict(data_dict)
write_csv(data_dict)
